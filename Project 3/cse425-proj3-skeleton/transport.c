/*
 * transport.c
 *
 *	Project 3
 *
 * This file implements the STCP layer that sits between the
 * mysocket and network layers. You are required to fill in the STCP
 * functionality in this file.
 *
 */


#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <arpa/inet.h>
#include "mysock.h"
#include "stcp_api.h"
#include "transport.h"


typedef enum
{
    CSTATE_SYN_SENT,
    CSTATE_SYN_RCVD,
    CSTATE_ESTABLISHED,
    CSTATE_FIN_WAIT_1,
    CSTATE_FIN_WAIT_2,
    CSTATE_LAST_ACK,
    CSTATE_CLOSE_WAIT,
    CSTATE_TIME_WAIT,
    CSTATE_CLOSE
} states;    /* you should have more states */


/* this structure is global to a mysocket descriptor */
typedef struct
{
    bool_t done;    /* TRUE once connection is closed */
    int connection_state;   /* state of the connection (established, etc.) */
    tcp_seq initial_sequence_num;

    /* any other connection-wide global variables go here */
    tcp_seq my_seq_num;     /* My current sequence number. Updated after sending packet to next usable number. */
    tcp_seq peer_seq_num;   /* Peer's last received seq num (So basically +1 this for ACK)*/

    tcp_seq last_acked_by_peer;     // Last ACKed by peer.
    tcp_seq last_byte_sent;     // Last "in-flight" byte.
    tcp_seq send_window_size;  /* Size of the sender's window */
    // last_byte_sent - last_acked_by_peer <= send_window_size
    tcp_seq receiver_advertized_window;     /*Size of receiver advertized window!*/
    tcp_seq receive_window_size;       /* Size of the receiver window! */
} context_t;


static void generate_initial_seq_num(context_t *ctx);
static void control_loop(mysocket_t sd, context_t *ctx);


/* initialise the transport layer, and start the main loop, handling
 * any data from the peer or the application.  this function should not
 * return until the connection is closed.
 */
void transport_init(mysocket_t sd, bool_t is_active)
{
    // Seed the random function.
    srand(time(NULL));
    context_t *ctx;

    ctx = (context_t *) calloc(1, sizeof(context_t));
    assert(ctx);

    generate_initial_seq_num(ctx);

    /* XXX: you should send a SYN packet here if is_active, or wait for one
     * to arrive if !is_active.  after the handshake completes, unblock the
     * application with stcp_unblock_application(sd).  you may also use
     * this to communicate an error condition back to the application, e.g.
     * if connection fails; to do so, just set errno appropriately (e.g. to
     * ECONNREFUSED, etc.) before calling the function.
     */
     /* Note that is_active is true is we are connecting and false if we are accepting.
     * So it will be true for the client (active end) and false for the server (passive end).
     */
    if(is_active)
    {
        // We are connecting. Send SYN packet here.
        struct tcphdr *header = (struct tcphdr *)malloc(sizeof(struct tcphdr));
        memset(header, 0, sizeof(struct tcphdr));
        // Set SYN flag and initial sequence number.
        header->th_flags |= TH_SYN;
        // Set advert window.
        header->th_win = htons(3072);
        header->th_seq = htonl(ctx->initial_sequence_num);
        header->th_off = 5;
        ctx->my_seq_num = htonl(ctx->initial_sequence_num);
        // printf("Sending SYN!\n");
        int len_send = stcp_network_send(sd, (void *)header, sizeof(struct tcphdr), NULL);
        // printf("SYN SENT with initseqnum=%d!\n", header->th_seq);
        // Change Connection State
        ctx->connection_state = CSTATE_SYN_SENT;
        ctx->my_seq_num ++;     // my_seq_num is the sequence number I will be using to send the next packet.
        if(!len_send)
        {
            perror("SYN sending error!");
        }
        // free(header);
        // Now wait for SYN-ACK!

        int event = stcp_wait_for_event(sd, NETWORK_DATA, NULL);
        if(event & NETWORK_DATA)
        {
            char buf[STCP_MSS];
            int len_recv = stcp_network_recv(sd, buf, STCP_MSS);
            if(!len_recv)
            {
                perror("SYN-ACK receiving error!");
            }

            header = NULL;
            header = (struct tcphdr *) buf;
            // printf("SYN received with initial sequence number %d\n", header->th_seq);
            // printf("Size of struct tcphdr is %d", sizeof(struct tcphdr));
            // Check for SYN and ACK flags!
            if((header->th_flags&TH_SYN) && (header->th_flags&TH_ACK))
            {
                // Get peer's sequence number.
                // We're keeping peer's number in a global context for the socket(in context_t).
                ctx->peer_seq_num = ntohl(header->th_seq);
                // Get advertized window!
                ctx->receiver_advertized_window = ntohs(header->th_win);
                ctx->receive_window_size = ntohl(header->th_win);
                // At this point, the above two values are the same.
                // ACK with peer's sequence number  + 1.
                struct tcphdr *sendingheader = (struct tcphdr *)malloc(sizeof(struct tcphdr));
                memset(sendingheader, 0, sizeof(struct tcphdr));
                sendingheader->th_flags = TH_SYN;
                sendingheader->th_seq = htonl(ctx->my_seq_num);
                ctx->my_seq_num++;
                sendingheader->th_ack = htonl(ctx->peer_seq_num+1);
                sendingheader->th_win = htons(3072);
                sendingheader->th_off = 5;
                int len_send_again = stcp_network_send(sd, (void *)sendingheader, STCP_MSS, NULL);
                if(len_send_again <= 0)
                {
                    perror("Did not send SYN to SYN-ACK for some reason!");
                }
                // Responded to SYN-ACK
                // free(sendingheader);
                // printf("Handshake Complete!\n");
            }
            else
            {
                perror("SYN-ACK expected, received something else!");
            }
        }
    }
    else
    {
        // We are accepting.
        // Waiting for SYN! Will respond with SYN-ACK
        char buf[STCP_MSS];
        struct tcphdr *header;
        int len_recv = stcp_network_recv(sd, buf, STCP_MSS);
        if(!len_recv)
        {
            // perror("Expected SYN, Nothing received!");
        }
        header = (struct tcphdr *) buf;
        // Check flags.
        // printf("Received SYN with sequence number %d\n", header->th_seq);
        if(header->th_flags & TH_SYN)
        {
            ctx->connection_state = CSTATE_SYN_RCVD;
            // SYN received! Send SYN-ACK
            // Get peer's sequence number.
            ctx->peer_seq_num = ntohl(header->th_seq);
            // Get advertized window!
            ctx->receiver_advertized_window = ntohs(header->th_win);
            ctx->receive_window_size = ntohl(header->th_win);
            // At this point, the above two values are the same.
            // ACK with sequence number peer's + 1.
            struct tcphdr *sendingheader = (struct tcphdr *)malloc(sizeof(struct tcphdr));
            memset(sendingheader, 0, sizeof(struct tcphdr));
            sendingheader->th_flags = TH_SYN | TH_ACK;
            sendingheader->th_seq = htonl(ctx->initial_sequence_num);
            // Set advertized window size.
            sendingheader->th_win = htons(3072);
            ctx->my_seq_num = ctx->initial_sequence_num + 1;
            sendingheader->th_ack = htonl(ctx->peer_seq_num+1);
            sendingheader->th_off = 5;
            int len_send_again = stcp_network_send(sd, (void *)sendingheader, STCP_MSS, NULL);
            // printf("Sent SYN with sequence number %d\n", sendingheader->th_seq);
            // printf("Sending ACK for sequence number %d\n", sendingheader->th_ack);
            if(len_send_again <= 0)
            {
                // perror("Did not send SYN to SYN-ACK for some reason!");
            }
            // Responded to SYN
            // struct tcphdr *ackrecv = (STCPHeader *)malloc(sizeof(STCPHeader));
            memset(buf, 0, STCP_MSS);
            stcp_network_recv(sd, buf, STCP_MSS);
            header = (struct tcphdr *) buf;
            ctx->peer_seq_num = ntohl(header->th_seq);
            // printf("Handshake complete!\n");
            // free(sendingheader);
        }
        else
        {
            // Acceptor should not get anything else.
            // perror("Waited for SYN, received something else!");
        }
    }
    ctx->connection_state = CSTATE_ESTABLISHED;
    stcp_unblock_application(sd);
    // printf("the context vars are:\n");
    // printf("ctx->my_seq_num = %ld\nctx->peer_seq_num = %ld\n", ctx->my_seq_num,  ctx->peer_seq_num);
    // printf("Application unblocked!\n");
    // printf("Entering control loop!\n");
    control_loop(sd, ctx);
    // printf("Exited control loop!\n");

    /* do any cleanup here */
    free(ctx);
}


/* generate random initial sequence number for an STCP connection */
static void generate_initial_seq_num(context_t *ctx)
{
    assert(ctx);

#ifdef FIXED_INITNUM
    /* please don't change this! */
    ctx->initial_sequence_num = 1;
#else
    /* you have to fill this up */
    ctx->initial_sequence_num =rand()%256;
#endif
}


/* control_loop() is the main STCP loop; it repeatedly waits for one of the
 * following to happen:
 *   - incoming data from the peer
 *   - new data from the application (via mywrite())
 *   - the socket to be closed (via myclose())
 *   - a timeout
 */
static void control_loop(mysocket_t sd, context_t *ctx)
{
    assert(ctx);
    assert(!ctx->done);

    while (!ctx->done)
    {
        // printf("Inside the loop :)\n");
        unsigned int event;

        /* see stcp_api.h or stcp_api.c for details of this function */
        /* XXX: you will need to change some of these arguments! */
        // printf("About to call stcp_wait_for_event!\n");
        event = stcp_wait_for_event(sd, 7, NULL);
        // printf("event = %d\n", event);
        /* check whether it was the network, app, or a close request */
        if (event & APP_DATA)
        {
            // printf("Sending data to peer\n");
            /* the application is sending data! */
            /* see stcp_app_recv() */
            /* Sliding window! */
            // Application has just done a mywrite().
            // Understanding : mywrite() puts data on app_recv_queue. We need to get that data
            // from the application. For this, we use stcp_app_recv() which dequeues from the mentioned queue.
            // Also, send the data to peer using stcp_network_send()
            // Step 1 : Get data using stcp_app_recv()
            // Step 2 : Prepare your headers. Get receiver's window and your seq num and stuff.
            // Step 3 : Send data!
            // Can I send another segment?
            if(ctx->my_seq_num - ctx->last_acked_by_peer <= ctx->send_window_size - STCP_MSS)
            {
                char *data = (char *)malloc(STCP_MSS*sizeof(char));
                memset(data, 0, STCP_MSS);
                int recvlen = stcp_app_recv(sd, data, STCP_MSS);
                struct tcphdr *header = (struct tcphdr *)malloc(sizeof(struct tcphdr));
                memset(header, 0, sizeof(STCPHeader));
                // Set headers!
                header->th_flags = TH_ACK;
                header->th_seq = htonl(ctx->my_seq_num);
                header->th_ack = htonl(ctx->peer_seq_num + 1);
                header->th_off = 5;
                header->th_win = htons(3072);
                // Now send the packet!
                // printf("Sending APP Data!\n");
                stcp_network_send(sd, (void *)header, sizeof(struct tcphdr), data, recvlen, NULL);
                ctx->my_seq_num = ctx->my_seq_num + recvlen;
                // free(header);
                // free(data);
            }
        }

        if (event & NETWORK_DATA)
        {
            /* the network is sending data! */
            /* see stcp_network_recv() */
            // Get data from network and send it to the application. You'll probably have to get rid of the headers.
            // Step 1 : Receive data. (Could be an ACK packet too!)
            // Step 2 : Send ACK packet! (remember to set the advertized window)
            // Step 3 : Strip the data of the headers
            // Step 4 : Use stcp_app_send() to send data to app (i.e., the appropriate queue).
            // printf("Expecting data from peer\n");
            int packsize = STCP_MSS + sizeof(struct tcphdr);
            char *packet = (char *)malloc(packsize*sizeof(char));
            memset(packet, 0, packsize);
            // printf("Receiving data!\n");
            // printf("Receiving\n");
            int recvlen = stcp_network_recv(sd, packet, packsize);
            // printf("Received %d bytes\n", recvlen);
            // printf("Received!\n");
            if(recvlen < 20)
            {
                break;
                printf("Serious Error, recvlen is %d!\n", recvlen);
            }
            struct tcphdr *recvheader = (struct tcphdr *)packet;
            int datasize = recvlen - sizeof(STCPHeader);
            char *data = packet + TCP_DATA_START(recvheader);
            if(datasize!=0)
            {
                // printf("About to send data to app with recvlen = %d and length = %ld\n", recvlen, datasize);
                stcp_app_send(sd, (void *)data, datasize);
                // printf("Data sent!\n");
            }
            // printf("ctx->peer_seq_num = %ld\nreceiver seq number = %ld\n",ctx->peer_seq_num, recvheader->th_seq);
            ctx->peer_seq_num = MAX(ctx->peer_seq_num, ntohl(recvheader->th_seq) + datasize - 1);
            if(recvheader->th_flags&TH_ACK)
            {
                    ctx->last_acked_by_peer = ntohl(recvheader->th_ack) - 1;
                    if(ctx->connection_state == CSTATE_FIN_WAIT_1)
                    {
                        ctx->connection_state = CSTATE_FIN_WAIT_2;
                    }
                    else if(ctx->connection_state == CSTATE_LAST_ACK)
                    {
                        ctx->connection_state = CSTATE_CLOSE;
                        ctx->done = TRUE;
                    }
            }
            if(recvheader->th_flags&TH_FIN)
            {
                stcp_fin_received(sd);
                if(ctx->connection_state == CSTATE_ESTABLISHED)
                {
                    ctx->connection_state = CSTATE_CLOSE_WAIT;
                }
                else if(ctx->connection_state == CSTATE_FIN_WAIT_2)
                {
                    ctx->connection_state = CSTATE_TIME_WAIT;
                }
            }

            // Set header fields
            struct tcphdr *ackheader = (struct tcphdr *)malloc(sizeof(struct tcphdr));
            memset(ackheader, 0, sizeof(struct tcphdr));
            ackheader->th_flags = TH_ACK;
            ackheader->th_seq = htonl(ctx->my_seq_num);
            ackheader->th_ack = htonl(ctx->peer_seq_num + 1);
            ackheader->th_off = 5;
            ackheader->th_win = htons(3072);
            // Send ACK!
            // printf("Sending ACK for %ld!\n", ackheader->th_ack);
            stcp_network_send(sd, (void *)ackheader, sizeof(struct tcphdr), NULL);
            // Have we just ACKed a FIN?
            if(recvheader->th_flags&TH_FIN)
            {
                if(ctx->connection_state == CSTATE_TIME_WAIT)
                {
                    ctx->connection_state = CSTATE_CLOSE;
                    ctx->done = TRUE;
                }
            }
            // printf("ACK Sent for %ld!!\n", ackheader->th_ack);
            // free(ackheader);
            // free(recvheader);
            // free(packet);
        }
        if (event & APP_CLOSE_REQUESTED)
        {
            // printf("In app_close_requested!\n");
            /* The application has requested to close the connection*/
            /* When the application requests that a mysocket is closed (via myclose()), your
            transport layer will receive a 'close requested event' from stcp_wait_for_event(); this
            is your cue to send a FIN segment once all pending data has been successfully
            transmitted. When you receive a FIN from the peer, and all expected data from the
            peer has been passed up to the application, you indicate that subsequent myread()s
            should return zero by calling stcp_fin_received().
            Also, remember to set the ctx->done field to true!*/
            /* Send fin segment */
            struct tcphdr *finhdr = (struct tcphdr *)malloc(sizeof(struct tcphdr));
            memset(finhdr, 0, sizeof(struct tcphdr));
            // Set fin flag and other parameters.
            finhdr->th_flags = TH_FIN | TH_ACK;
            finhdr->th_seq = htonl(ctx->my_seq_num);
            finhdr->th_ack = htonl(ctx->peer_seq_num+1);
            finhdr->th_off = 5;
            finhdr->th_win = htons(3072);
            // Send FIN to peers.
            stcp_network_send(sd, (void *)finhdr, sizeof(STCPHeader), NULL);
            if(ctx->connection_state == CSTATE_ESTABLISHED)
            {
                ctx->connection_state = CSTATE_FIN_WAIT_1;
            }
            else if(ctx->connection_state == CSTATE_CLOSE_WAIT)
            {
                ctx->connection_state = CSTATE_LAST_ACK;
            }
        }
    }
}


/**********************************************************************/
/* our_dprintf
 *
 * Send a formatted message to stdout.
 *
 * format               A printf-style format string.
 *
 * This function is equivalent to a printf, but may be
 * changed to log errors to a file if desired.
 *
 * Calls to this function are generated by the dprintf amd
 * dperror macros in transport.h
 */
void our_dprintf(const char *format,...)
{
    va_list argptr;
    char buffer[1024];

    assert(format);
    va_start(argptr, format);
    vsnprintf(buffer, sizeof(buffer), format, argptr);
    va_end(argptr);
    fputs(buffer, stdout);
    fflush(stdout);
}
