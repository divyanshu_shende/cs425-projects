#include "proxy_parse.h"
// #include "proxy_parse.c"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/stat.h> //To check for file in directory!
#include <unistd.h>
#include <arpa/inet.h>
#include <signal.h>
#include <fcntl.h>
#include <dirent.h>
#include <netdb.h>

typedef struct ParsedRequest ParsedRequest;
#define BUF_LEN 100000
#define MAX_CONN 100000
char serverbuf[BUF_LEN];
char clientbuf[BUF_LEN];
int serv_socket;
int proxy_socket;
int PROXY_PORT;

// The printf's are a part of the comments in submission on purpose.
int main(int argc, char * argv[])
{
    struct sockaddr clientAddr;
	struct sockaddr_in proxyAddr;
    int client;
    if(argc != 2)
    {
        printf("usage : ./proxy port-number\n");
        exit(1);
    }
    else
    {
        PROXY_PORT = atoi(argv[1]);
    }

    proxyAddr.sin_family = AF_INET;
    proxyAddr.sin_port = htons(PROXY_PORT);
    // Bind to all interfaces, not just localhost. This means bind to any IP address the machine has.
    // This is a point of difference from previous project/assignment.
    proxyAddr.sin_addr.s_addr = INADDR_ANY;

    int proxy_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

    bind(proxy_socket, (struct sockaddr *)&proxyAddr, sizeof(proxyAddr));
    listen(proxy_socket, MAX_CONN);
    memset(clientbuf, '\0', BUF_LEN);
    while(1)
    {
        pid_t pid;
        int addr_len = sizeof(clientAddr);
        client = accept(proxy_socket, (struct sockaddr *)&clientAddr, &addr_len);
        if(client < 0)
        {
            exit(0);
        }
        // Fork a child to handle connection!
        pid = fork();
        if(pid == 0)
        {
            // printf("Client Connected!\n");
            memset(clientbuf, '\0', BUF_LEN);
            int k = recv(client, clientbuf, BUF_LEN, 0);
            if(k<0)
            {
                shutdown(client, 2);
                exit(1);
            }
            ParsedRequest *req;
            req = ParsedRequest_create();
            //printf("Sent to parse()\n");
            int parseflag = ParsedRequest_parse(req, clientbuf, strlen(clientbuf));
            if(req->port == NULL)
            {
                // Case : No Port specified!
                req->port = malloc(BUF_LEN*sizeof(char));
                //printf("It's NULL\n");
                char tmpport[]="80";
                memset(req->port, '\0', sizeof(req->port));
                strcpy(req->port, tmpport);
                //printf("The port has been changed!\n");
            }
            // printf("The method used is %s\n", req->method);
            // printf("The host is %s\n", req->host);
            // printf("The port is %s\n", req->port);
            // printf("The path is %s\n", req->path);
            struct hostent *hostaddr;
            hostaddr = gethostbyname(req->host);
            struct sockaddr_in serverAddr;
            serverAddr.sin_family = AF_INET;
            serverAddr.sin_port = htons(atoi(req->port));
            memcpy(&serverAddr.sin_addr.s_addr, hostaddr->h_addr, hostaddr->h_length);
            int serv_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
            int check_serv = connect(serv_socket, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
            if(check_serv < 0)
                exit(1);
            memset(serverbuf, '\0', BUF_LEN);
            // Check path
            // I am sending a very limited and minimal request to the server. I have compromised in the proxy.h
            // file so that it could now possibly allow a little loose requests. Therefore, I have decided to send
            // the bare minimum info to the server.
            sprintf(serverbuf,"GET %s %s\r\nHost : %s\r\nConnection : close\r\n\r\n", req->path, req->version, req ->host);
            // printf("Path is : %s\n", req->path);
            // printf("We're sending : \n");
            // printf("%s\n", serverbuf);
            // Send request!
            int n = send(serv_socket, serverbuf, strlen(serverbuf), 0);
            if(n<0)
            {
                // Cannot connect to server
                exit(1);
            }
            n = 1;
            // Key is to keep receiving from the server. The pages could possibly send a lot of data! Keep forwarding
            // to client. Also, do not forward to client if the server quits. Otherwise, browser gives issues.
            while(n>0)
            {
                memset(serverbuf, '\0',  BUF_LEN);
                n = recv(serv_socket, serverbuf, BUF_LEN, 0);
                // printf("Received this :\n");
                // printf("%s\n", serverbuf);
                if(n > 0)
                {
                    send(client, serverbuf, n, 0);
                    // printf("Sent this :\n");
                    // printf("%s\n", serverbuf);
                }
            }
            // Close all sockets for this process and get out of here!
            close(serv_socket);
			close(client);
            close(proxy_socket);
            exit(0);
        }
        else
        {
            // Parent closes it's client connection. This allows to listen for more requests.
            // Also, the code would not run properly on my machine without the continue. WIERD!?
            close(client);
            continue;
        }
    }
    return 0;
}
