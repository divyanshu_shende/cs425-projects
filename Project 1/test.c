#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "proxy_parse.c"
#include "proxy_parse.h"


#define BUF_LEN 1024
#define MAX_CONN 10
int SERVER_PORT = 5252;
char buf[BUF_LEN];
int serv_socket;
int client;

int main()
{
    // create buffer for sending and receiving messages!
	struct sockaddr_in serverAddr;
  	// Ignore signal to kill server if client quits.
  	signal(SIGPIPE,SIG_IGN);

  	// Initialize buffer
    memset(buf, '\0', BUF_LEN);
    // Creating server socket
    serv_socket=socket(PF_INET, SOCK_STREAM, 0);
	// Configuring address structure for the bind() for server.
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(SERVER_PORT);
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    bind(serv_socket, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
    // int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
    while(1)
    {
	    printf("Waiting for connection.\n");
	    listen(serv_socket, 5);
	    int addr_len = sizeof(serverAddr);
	    client = accept(serv_socket, (struct sockaddr *)&serverAddr, &addr_len);
		printf("Connection Established!\n");
		// Create a child process to listen to this connection. Parent looks for another call.
		if(fork()==0)
		{
			while(1)
			{
				int l = recv(client, buf, BUF_LEN, 0);
				if(l <= 0)
				{
					break;
				}
				// At this point, request is in buf, we now need to parse it.
				parse_request();
				// request is now parsed. We now need to respond to it.
				service_request();
			}
		}
	}
    return 0;
}
