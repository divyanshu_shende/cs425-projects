#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/stat.h> //To check for file in directory!
#include <unistd.h>
#include <arpa/inet.h>
#include <signal.h>
#include <fcntl.h>
#include "parsing.c"
#include <dirent.h>

#define BUF_LEN 100000
#define MAX_CONN 100000
char sockbuf[BUF_LEN];
int serv_socket;
int client[MAX_CONN];
int SERVER_PORT;

void create_response(parse_res *res, int n);	// Creates the response to be sent to and sends the response to client.
void request_handling(int n);	// Convenience function to simplify code. Essentially handles request logisticaly.

int main(int argc, char *argv[])
{
	// create sockbuffer for sending and receiving messages!
	struct sockaddr clientAddr;
	struct sockaddr_in serverAddr;
	if(argc!=2)
    {
      printf("usage : ./server port-number\n");
      exit(1);
    }
    else
      SERVER_PORT = atoi(argv[1]); //Read port number from argv and convert to int
 	// Initialize sockbuffer
    memset(sockbuf, '\0', BUF_LEN);
    // Creating server socket
    serv_socket=socket(PF_INET, SOCK_STREAM, 0);
	// Configuring address structure for the bind() for server.
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(SERVER_PORT);
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    bind(serv_socket, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
	int n = 0;
	if ( listen (serv_socket, 1000000) != 0 )
    {
        perror("listen() error");
        exit(1);
    }
	int initial;
	// Initialize all clients.
	for(initial = 0; initial < MAX_CONN; initial++)
	{
		client[initial] = -1;
	}
    // int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
    while(1)
    {
		int addr_len = sizeof(clientAddr);
		// Create connection for client n
		client[n] = accept(serv_socket, (struct sockaddr *)&clientAddr, &addr_len);
		// If connection is invalid then exit!
		if (client[n] < 0)
		{
			exit(1);
		}
		else // if connection is valid!
		{
			// Fork a child to handle requests. Parent listens for further connections.
			// Important : Child will exit() after it services request. It will never go outside this else.
 			if (fork()==0)
            {
				// printf("n is %d and client[n] %d\n", n, client[n]);
				if (client[n] < 0)
				{
					exit(1);
				}
				// Receive requests
				int k = recv(client[n], sockbuf, BUF_LEN, 0);
				if(k<0)
					break;
				// Send request for handling
				request_handling(n);
				// Close this socket! Also, note that this child exits after handling request.
				shutdown(client[n], 2);
				exit(0);
            }
		}
		// Parent keeps track of which clients have exited. Note that this is the main parent that started the program.
		while (client[n] != -1) {
			n = (n+1)%MAX_CONN;
		}
	}
  return 0;
}

// This is a logistic function. The comments in the form of printf are useful for both debugging and understanding purposes.
void request_handling(int n)
{
	// printf("Sending for parsing\n");
	parse_req *req = create_parse_req();
	// printf("Parser object created!\n");
	parse(req, sockbuf, BUF_LEN);
	// printf("parse() function exited!\n");
	// printf("The method used is %s\n", req->method);
	// printf("The file requested is %s\n", req->path);
	// printf("Connection type is %s\n", req->keepalive);
	parse_res *res=create_parse_res();
	// printf("Created response object!\n");
	service_request(req, res);
	// printf("Request serviced!\n");
	create_response(res, n);
	// printf("Response created and request sent!\n");
	destroy_parser(req, res);
	memset(sockbuf, '\0', BUF_LEN);
}


void create_response(parse_res *res, int n)
{
    char *resline = malloc(BUF_LEN*sizeof(char));
    if(res->statuscode == 200)
        resline = "HTTP/1.1 200 OK\r\n";
    else
	{
		// printf("Processing 404 request!\n");
		char error_resp[]="<html><body><h1>404! File not Found!</h1></body></html>";
		send(client[n], "HTTP/1.1 404 Not Found\n", 24, 0);
		send(client[n], "Content-length: 46\n", 19, 0);
		send(client[n], "Content-Type: text/html\n\n", 25, 0);
		send(client[n], error_resp, strlen(error_resp), 0);
		return ;
	}
    char *headers = malloc(BUF_LEN*sizeof(char));
	// Look at below snprintf argument to see what we're trying to do to the headers
    // snprintf(headers, 1000, "%s\n\nContent-Length : %s\n\nContent-Type : %s\n\nConnection : Close\n\\n", resline, res->clen, res->ctype);
	strcpy(headers, resline);
    strcat(headers, "Content-Length : ");
    strcat(headers, res->clen);
	strcat(headers, "\r\n");
    strcat(headers, "Content-Type : ");
    strcat(headers, res->ctype);
	strcat(headers, "\r\n\r\n");
	int l = strlen(headers);
	send(client[n], headers, l, 0);
	// Headers have been looked at.
	int next, fd;
	//sending potentially large data in chunks.
	int chunk_size = 1024;
	char *chunk = malloc(chunk_size*sizeof(char));
	memset(chunk, '\0', chunk_size);
	// printf("sending data\n");
	char filepath[1000];
	strcpy(filepath, res->filename);
	if(res->dir_req == 1)
	{
		send(client[n], res->body, strlen(res->body), 0);
		return ;
	}
	int k = 0;
	// Find file and read from it!
	if ((fd=open(filepath, O_RDONLY)) != -1)
	{
		while ( (next=read(fd, chunk, chunk_size)) > 0 )
		{
			// printf("%s\n", chunk);
			send(client[n], chunk, next, 0);
		}
	}
    // res->finalLen = fileLen+strlen(headers);
    // return reply;
	return ;
}
