// This file contains functions that'll parse http requests.
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/stat.h> //To check for file in directory!
#include <unistd.h>
#include <arpa/inet.h>
#include <signal.h>
#include <fcntl.h>
#include <netdb.h>
#include <dirent.h>

#define BUF_LEN 100000

// Define a structure to store requsts from clients. The fields store values designated by name.
// This is based on "proxy_parse.c/h" sent in Project 2
typedef struct parse_req{
    int occupied;
    char *URI;
    char *method;
    char *protocol;
    char *host;
    char *port;
    char *path;
    char *keepalive;
    char *buf;
} parse_req;

// The response is also stored and maintained in a structure. The fields have obvious meanings except those mentioned below.
// dir_req is used to keep track of whether or not a directory has been requested.
typedef struct parse_res{
    int statuscode;
    char *body;
    char *clen;
    char ctype[20];
    char *keepalive;
    char *filename;
    int dir_req;
} parse_res;

parse_req* create_parse_req();          // Creates and initializes request object
int parse_req_line(parse_req *req, char *tmpbuf, int buflen);       // Parses the request line in request message
void parse(parse_req *req, char *sockbuf, int buflen);              // The parser function for request message, this assigns value to the request object. It calls parse_req_line() too.
int parse_header_fn(parse_req *req, char *sockbuf, int buflen);     // Parses request message headers, if any.
parse_res* create_parse_res();          // Creates and initializes response object
void service_request(parse_req *req, parse_res *res);   // Takes the request object and fills in the response object with appropriate data.
void destroy_parser(parse_req *req, parse_res *res);    // Destroys the request and response objects and frees their memory.
int listdir(char *dirname, parse_res *res);             // Used when we want to list contents of directory.

char *reqend;

// Create and Initialize request object and return pointer.
parse_req* create_parse_req()
{
    parse_req *tmp;
    tmp = malloc(sizeof(parse_req));
    tmp->URI = NULL;
    tmp->method = NULL;
    tmp->protocol = NULL;
    tmp->host = NULL;
    tmp->port = NULL;
    tmp->path = NULL;
    tmp->keepalive = NULL;
    tmp->buf = NULL;
    tmp->occupied=0;
    return tmp;
}

// Parse the request
void parse(parse_req *req, char *sockbuf, int buflen)
{
    if(req->occupied!=0)
    {
        // printf("Parse object already in use!\n");
        exit(1);
    }
    // Copy request from sockbuf to req->buf.
    req->buf = malloc(buflen*sizeof(char));
    strcpy(req->buf, sockbuf);
    char *tmpbuf = malloc(buflen * sizeof(char));
    strcpy(tmpbuf, sockbuf);
    // printf("Sending to parse_line_req() to parse request line!\n");
    int rl = parse_req_line(req, tmpbuf, buflen);
    // If parsing was unsuccessful, then we quit!
    if(!rl)
    {
        free(req);
        free(tmpbuf);
        exit(0);
    }
    return ;
}

// Parse request line!
int parse_req_line(parse_req *req, char *tmpbuf, int buflen)
{
    // The below code essentially splits the request line into tokens and deciphers request.
    char *newbuf = malloc(buflen * sizeof(char));
    int i;
    reqend = strstr(tmpbuf, "\r\n");
    char *ptr;
    for(i=0;i<buflen && ptr!=reqend;i++)
    {
        ptr = &tmpbuf[i];
        newbuf[i] = tmpbuf[i];
    }
    char *token = strtok(newbuf, " ");
    if(token == NULL)
    {
        printf("Method not entered!\n");
        free(newbuf);
        return 0;
    }
    req->method = token;
    token = strtok(NULL, " ");
    if(token == NULL)
    {
        printf("URI not entered\n");
        free(newbuf);
        return 0;
    }

    req->path = token;
    token = strtok(NULL, " ");
    if(!strcmp(token, "HTTP/1.1"))
    {
        printf("Version Error\n");
        free(newbuf);
        return 0;
    }
    free(newbuf);
    return 1;
}


// This function looks for connection headeer.
int parse_header_fn(parse_req *req, char *sockbuf, int buflen)
{
    int flag=0;
    char *newbuf = malloc(buflen * sizeof(char));
    char *tmpbuf = malloc(buflen * sizeof(char));
    strcpy(newbuf, sockbuf);
    char *token = strtok(newbuf, "\n");
    while(token!=NULL)
    {
        char *divide = strstr(token, ":");
        if(divide==NULL)
            continue;
        char conn[]="Connection";
        // Connection field
        if(strncmp(conn, token, sizeof(conn)) == 0)
        {
            flag = 1;
            if(strstr(token, "-")==NULL)
                strcpy(req->keepalive, "Close");
            else
                strcpy(req->keepalive, "Keep-Alive");
        }
        token = strtok(NULL, "\r\n");
    }
    if(!flag)
    {
        return 0;
        // printf("Connection header not sent by client!\n");
    }
    return 0;
}

// Create and initialize response object and return pointer.
parse_res* create_parse_res()
{
    parse_res *tmp;
    tmp->statuscode=0;
    tmp->body=malloc(BUF_LEN*sizeof(char));
    tmp->clen=NULL;
    tmp->keepalive=NULL;
    tmp->filename=NULL;
    tmp->dir_req=0;
    return tmp;
}

// Start processing request.
void service_request(parse_req *req, parse_res *res)
{
	char *fname = malloc(BUF_LEN*sizeof(char));
    char *dname = malloc(BUF_LEN*sizeof(char));
	strcpy(fname, req->path);
    // save directory name for checking later!
    strcpy(dname, req->path);
    int poss_dir=0; // Checks for possibility of a directory request
    if(strstr(fname, ".")==NULL)       // Checking for . to denote directory request
    {
        poss_dir = 1;           // Possible directory request
        char tfname[]="index.html";
        strcat(fname, tfname);
    }
    // Remove starting slash.
    char *remslash=fname;
    remslash = remslash + 1;
    // printf("Copying filename to %s\n", remslash);
    strcpy(fname, remslash);
    remslash = dname;
    remslash = remslash + 1;
    strcpy(dname, remslash);
    // Add ./ to directory name. Eg, webfiles becomes ./webfiles
    char *adddot = malloc(BUF_LEN*sizeof(char));
    strcpy(adddot, "./");
    strcat(adddot, dname);
    strcpy(dname, adddot);
    res->filename = malloc(1000*sizeof(char));
    strcpy(res->filename, fname);
	// Using stat to check if file exists in current directory.
	// stat returns 0 if file exists, -1 otherwise.
	struct stat sb;
	int n_send;
	int checkfile = stat(fname, &sb);
    // printf("stat() returned!\n");
    if(checkfile == -1)
    {
        if(poss_dir == 1)
        {
            struct stat sd;
            // Check for anything with name = dname.
            int checkdir = stat(dname, &sd);
            if(S_ISDIR(sd.st_mode))
            {
                // printf("Directory checked!\n");
                res->dir_req = 1;
                if(listdir(dname, res)==200)
                {
                    // Create response for this type
                    res->statuscode = 200;
                    strcpy(res->ctype, "text/plain");
                    int l = strlen(res->body);
                    char dirstr[10];
                    snprintf(dirstr, 10, "%d", l);
                    res->clen = malloc(strlen(dirstr)*sizeof(char));
                    strcpy(res->clen, dirstr);
                    return ;
                }
            }
            else
            {
                // printf("%s directory does not exist\n", dname);
            }
        }
        else
        {
        // printf("statuscode 404 confirmed!\n");
        res->statuscode = 404;
        return ;
        }
    }
    else
    {
        res->statuscode = 200;
    }
    // Now we open the file and read from it.
    FILE *fp = fopen(fname, "rb");
    fseek(fp, 0L, SEEK_END);
    int fsize = ftell(fp);
    rewind(fp);
    fread(res->body, fsize, 1, fp);
    fseek(fp, 0, SEEK_SET);
    fclose(fp);
    // Convert length to string.
    char istr[10];
    snprintf(istr, 10, "%d", fsize);
    res->clen = malloc(strlen(istr)*sizeof(char));
    strcpy(res->clen, istr);
    if(strstr(fname, ".htm")!=NULL)
    {
        // printf("Case html\n");
        strcpy(res->ctype, "text/html");
    }
    else if(strstr(fname, ".txt")!=NULL)
    {
        // printf("Case txt\n");
        strcpy(res->ctype, "text/plain");

    }
    else if(strstr(fname, ".jpeg")!=NULL || strstr(fname, ".jpg")!=NULL)
    {
        // printf("Case jpg/jpeg\n");
        strcpy(res->ctype, "image/jpeg");
    }
    else if(strstr(fname, ".gif")!=NULL)
    {
        // printf("Case gif\n");
        strcpy(res->ctype, "image/gif");
    }
    else if(strstr(fname, ".pdf")!=NULL)
    {
        // printf("Case pdf\n");
        strcpy(res->ctype, "Application/pdf");
    }
    else
    {
        // printf("Case neither : octet!\n");
        strcpy(res->ctype, "application/octet-stream");
    }
    free(fname);
    free(dname);
    // printf("Exiting service_request()\n");
    return ;
}

void destroy_parser(parse_req *req, parse_res *res)
{
    free(req);
    free(res);
    return ;
}

int listdir(char *dirname, parse_res *res)
{
    struct dirent *entry;
    DIR* mydir = opendir(dirname);
    // success will store the return code. 200 if directory exists, 404 otherwise.
    int success;
    memset(res->body, '\0', BUF_LEN);
    strcpy(res->body, "Contents of this directory are : \n");
    if(mydir!=NULL)
    {
        while(entry = readdir(mydir))   //Returns NULL when all files are listed
        {
            // entry->d_name has name of file
            // we skip . and .. since they are trivial directories
            if((strcmp(entry->d_name, ".") == 0) || (strcmp(entry->d_name, "..") == 0))
                continue;
            strcat(res->body, entry->d_name);
            strcat(res->body, "\n");
        }
        closedir(mydir);
        success = 200;
    }
    else
    {
        success = 404;
    }
    return success;
}
